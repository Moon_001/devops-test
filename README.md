# Go Application

```
cd local_repo
git remote add origin https://gitlab.com/Moon_001/devops-test.git

```

## Prerequisites

- [Docker](https://www.docker.com/)
- [Minikube](https://minikube.sigs.k8s.io/docs/start/) (or another Kubernetes cluster)
- [Helm](https://helm.sh/) (for Exercise 3)

## Exercise 0 - Docker

1. Open a terminal.

2. Navigate to exo0 branch: `git checkout exo0`.

3. Run the unit tests: `go test`.

4. Build the Docker image: `docker build -t hello-world-app .`.

5. Launch the application using Docker. Assure that the port 8080 is not already in use: `docker run -p 8080:8080 hello-world-app`.

## Exercise 1 - GitLab CI



1. Navigate to exo1 branch: `git checkout exo1`.

2. Ensure you have a local GitLab runner named "BobTheBuilder" in the config.toml.

3. the `.gitlab-ci.yml` file run tests and build the Docker image in a pipeline.


## Exercise 2 - Kubernetes

1. Start local Kubernetes cluster , you need to install it first `minikube start`.

2. Navigate to exo2 branch: `git checkout exo2`.

3. Apply the deployment files: `kubectl apply -f deployment.yml` , `kubectl apply -f service.yml`.

## Exercise 3 - Helm

1. Make sure Helm is installed: `helm version`.

2. Navigate to exo3 branch: `git checkout exo3`.

3. Install the Helm charts: `helm install .my-hello-world-chart/`.

## Exercise 4 - Auto-deploy

1. Navigate to exo4 branch: `git checkout exo4`.

2. Assuring that the local kubernetes is running by doing `minikube status` the `.gitlab-ci.yml` file run tests and build the Docker image an deploy it in a pipeline. Assure to set well the network configuration pf your local machine before run rhe pipeline

